# article-duplicate

 **文章重复检测小工具，可以用来找到被盗用的文章。** 

 **使用方法：** 

clone代码，运行main


```
        //chrome驱动 与安装的chrome版本需一致
        String path = Application.class.getClassLoader().getResource("chromedriver.exe").getPath();
        System.setProperty("webdriver.chrome.driver", path);

        DeDup deDup = new DeDupDefaultImpl();
        Input input = new Input();
        //文章标题
        input.setKey("标题");
        //查重内容
        input.setContent("内容");
        //查重级别
        input.setLevel(1);
        //最大重复率，超过该值时任务是重复的
        input.setMaxRepetitionRate(0.9);
        //url白名单，不会获取白名单中的内容
        //Set<String> whiteUrls = new HashSet<>();
        //whiteUrls.add("");
        //whiteUrls.add("");
        //input.setWhiteUrls(whiteUrls);

        deDup.setInput(input);

        Output output = deDup.getOutput();

        System.out.println(JSONObject.toJSONString(output.getResult()));
```

 **原理：** 

selenium调用chrome driver进行百度搜索，获取对应的网页内容，分段后计算文本相似度。相似度算法为余弦相似。

 **检测结果：** 


```
{
    "resultSentence":{
        "sentenceMap":{
            "7的环境，遇到的问题还不是很多，还算比较顺利":[
                {
                    "context":"...境搭建记录及遇到的问题 0. 初衷 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1.1的环境在自己的机器上来玩一玩，虽然去年也勉...",
                    "sentence":"7的环境，遇到的问题还不是很多，还算比较顺利",
                    "similarity":1.0000000000000002,
                    "url":"https://www.cnblogs.com/cnsec/p/13286604.html"
                },
                {
                    "context":"...次浏览 分类：学习教程 0. 初衷 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1.1的环境在自己的机器上来玩一玩，虽然去年也勉...",
                    "sentence":"7的环境，遇到的问题还不是很多，还算比较顺利",
                    "similarity":1.0000000000000002,
                    "url":"http://www.luyixian.cn/news_show_311696.aspx"
                },
                {
                    "context":"...CSDN博客 2020-08-26 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境,遇到的问题还不是很多,还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1.1的环境在自己的机器上... 最全ambar...",
                    "sentence":"7的环境,遇到的问题还不是很多,还算比较顺利",
                    "similarity":1.0000000000000002,
                    "url":"https://download.csdn.net/download/ToBeATree_/12446928"
                },
                {
                    "context":"...到的问题 6342020-03-29前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1.1的环境在自己的机器上来玩一玩，虽然去年也勉...",
                    "sentence":"7的环境，遇到的问题还不是很多，还算比较顺利",
                    "similarity":1.0000000000000002,
                    "url":"https://download.csdn.net/download/ToBeATree_/12446928"
                },
                {
                    "context":"...问题 标签： 开发笔记 0. 初衷 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1.1的环境在自己的机器上来玩一玩，虽然去年也勉...",
                    "sentence":"7的环境，遇到的问题还不是很多，还算比较顺利",
                    "similarity":1.0000000000000002,
                    "url":"https://www.freesion.com/article/2607401273/"
                }
            ],
            "前段时间在公司服务器上重新搭了一套ambari2":[
                {
                    "context":"... 文章 - 0 评论 - 0 ambari2.7.3+hdp3.1.1环境搭建记录及遇到的问题 0. 初衷 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3...",
                    "sentence":" 初衷 前段时间在公司服务器上重新搭了一套ambari2",
                    "similarity":0.9636241116594316,
                    "url":"https://www.cnblogs.com/cnsec/p/13286604.html"
                },
                {
                    "context":"...录及遇到的问题 2020/3/29 14:51:43 0 人评论 83 次浏览 分类：学习教程 0. 初衷 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3...",
                    "sentence":" 初衷 前段时间在公司服务器上重新搭了一套ambari2",
                    "similarity":0.9636241116594316,
                    "url":"http://www.luyixian.cn/news_show_311696.aspx"
                },
                {
                    "context":"...章阅读平台 ambari2.7.3+hdp3.1.1环境搭建记录及遇到的问题 标签： 开发笔记 0. 初衷 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3...",
                    "sentence":" 初衷 前段时间在公司服务器上重新搭了一套ambari2",
                    "similarity":0.9636241116594316,
                    "url":"https://www.freesion.com/article/2607401273/"
                }
            ],
            "6+hdp2":[
                {
                    "context":"...p3.1.1环境搭建记录及遇到的问题 0. 初衷 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1...",
                    "sentence":"6+hdp2",
                    "similarity":1,
                    "url":"https://www.cnblogs.com/cnsec/p/13286604.html"
                },
                {
                    "context":"...人评论 83 次浏览 分类：学习教程 0. 初衷 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1...",
                    "sentence":"6+hdp2",
                    "similarity":1,
                    "url":"http://www.luyixian.cn/news_show_311696.aspx"
                },
                {
                    "context":"...隐秘的..._CSDN博客 2020-08-26 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境,遇到的问题还不是很多,还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1...",
                    "sentence":"6+hdp2",
                    "similarity":1,
                    "url":"https://download.csdn.net/download/ToBeATree_/12446928"
                },
                {
                    "context":"...隐秘的..._CSDN博客 2020-08-26 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境,遇到的问题还不是很多,还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1...",
                    "sentence":"6+hdp2",
                    "similarity":1,
                    "url":"https://download.csdn.net/download/ToBeATree_/12446928"
                },
                {
                    "context":"...建记录及遇到的问题 标签： 开发笔记 0. 初衷 前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。现在想再搭建一套ambari2.7.3+hdp3.1...",
                    "sentence":"6+hdp2",
                    "similarity":1,
                    "url":"https://www.freesion.com/article/2607401273/"
                }
            ]
        }
    },
    "similarity":1.018181818181818
}
```

