package com.yao2san.dedup;


import com.alibaba.fastjson.JSONObject;
import com.yao2san.dedup.crawler.DeDup;
import com.yao2san.dedup.crawler.DeDupDefaultImpl;
import com.yao2san.dedup.input.Input;
import com.yao2san.dedup.output.Output;

import java.util.HashSet;
import java.util.Set;

/**
 * 重复度检测
 */
public class Application {

    public static void main(String[] args) {
        //chrome驱动 与安装的chrome版本需一致
        String path = Application.class.getClassLoader().getResource("chromedriver.exe").getPath();
        System.setProperty("webdriver.chrome.driver", path);

        DeDup deDup = new DeDupDefaultImpl();
        Input input = new Input();
        //文章标题
        input.setKey("ambari2.7.3+hdp3.1.1环境搭建记录及遇到的问题");
        //查重内容
        input.setContent("前段时间在公司服务器上重新搭了一套ambari2.6+hdp2.7的环境，遇到的问题还不是很多，还算比较顺利。");
        //查重级别
        input.setLevel(1);
        //最大重复率，超过该值时任务是重复的
        input.setMaxRepetitionRate(0.9);
        //url白名单，不会获取白名单中的内容
        Set<String> whiteUrls = new HashSet<>();
        whiteUrls.add("https://blog.csdn.net/wxgxgp");
        whiteUrls.add("https://www.csdn.net");
        input.setWhiteUrls(whiteUrls);

        deDup.setInput(input);

        Output output = deDup.getOutput();

        System.out.println(JSONObject.toJSONString(output.getResult()));
    }
}
