package com.yao2san.dedup.crawler;

import com.yao2san.dedup.input.Input;
import com.yao2san.dedup.output.Output;

public interface DeDup {

    void setInput(Input input);

    Output getOutput();
}
