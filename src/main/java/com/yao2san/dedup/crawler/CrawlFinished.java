package com.yao2san.dedup.crawler;

import org.jsoup.nodes.Document;

public interface CrawlFinished {
    void finished(String url, Document document);
}
