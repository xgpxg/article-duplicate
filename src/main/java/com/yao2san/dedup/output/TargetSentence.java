package com.yao2san.dedup.output;

import lombok.Data;

@Data
public class TargetSentence {
    private String sentence;
    private double similarity;
    private String url;
    private String context;
}
