package com.yao2san.dedup.output;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SourceSentence {
    private String sentence;

    @Override
    public int hashCode() {
        return sentence.hashCode();
    }
}
