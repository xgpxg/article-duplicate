package com.yao2san.dedup.output;

import com.yao2san.dedup.input.Input;
import lombok.Data;

@Data
public class Output {
    private Input input;
    private Result result;
}
