package com.yao2san.dedup.output;

import lombok.Data;

@Data
public class Result {
    private ResultSentence resultSentence;
    private double similarity;
}
