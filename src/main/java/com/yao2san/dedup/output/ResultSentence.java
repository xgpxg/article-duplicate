package com.yao2san.dedup.output;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class ResultSentence {
    private Map<String, List<TargetSentence>> sentenceMap;
    public ResultSentence(){

    }
    public ResultSentence(Map<String, List<TargetSentence>> sentenceMap) {
        this.sentenceMap = sentenceMap;
    }

    public Set<String> sourceSentenceSet() {
        return this.sentenceMap.keySet();
    }

    public List<TargetSentence> targetSentences(String sourceSentence) {
        return this.targetSentences(new SourceSentence(sourceSentence));
    }

    public List<TargetSentence> targetSentences(SourceSentence sourceSentence) {
        return sentenceMap.get(sourceSentence);
    }
}
