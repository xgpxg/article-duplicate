package com.yao2san.dedup.input;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
public class Input {
    /**
     * 关键字
     */
    private String key;
    /**
     * 内容
     */
    private String content;
    /**
     * 去重级别(即参照的页数)
     */
    private Integer level = 1;

    private Double maxRepetitionRate = 0.3;

    private List<String> sentences;

    /**
     * url白名单(前缀匹配)
     */
    private Set<String> whiteUrls;


    static int d = 0;

    private static int dis(String s1, String s2, int i, int j) {
        if (i == 0 || j == 0) {
            return Math.max(i, j);
        }
        int op1 = dis(s1, s2, i - 1, j) + 1;
        int op2 = dis(s1, s2, i, j - 1) + 1;
        int op3 = dis(s1, s2, i - 1, j - 1);
        if (s1.charAt(i - 1) != s2.charAt(j - 1)) {
            op3 += 1;
        }
        return Math.min(Math.min(op1, op2), op3);
    }

    private static int dis(String s1, String s2) {
        //int[][] temp = new int[s1.length()][s2.length()];
        List<List<Integer>> tpl = new ArrayList<>();
    /*    for (int i = 0; i < s1.toCharArray().length; i++) {
            //temp[i][0] = i;
            List<Integer> list = new ArrayList<>();
            list.add(i);
            tpl.add(list);

        }

        for (int j = 1; j < s2.toCharArray().length; j++) {
            //temp[0][j] = j;
            tpl.get(j).add(j);
        }*/
        for (int i = 0; i < s1.toCharArray().length; i++) {
            List<Integer> list = new ArrayList<>();

            for (int j = 0; j < s2.toCharArray().length; j++) {
                list.add(j);
            }
            tpl.add(list);
        }
        for (int i = 1; i < s1.toCharArray().length; i++) {
            for (int j = 1; j < s2.toCharArray().length; j++) {
                if (s1.charAt(i) == s2.charAt(j)) {
                    //temp[i][j] = temp[i - 1][j - 1];
                    tpl.get(i).set(j, tpl.get(i - 1).get(j - 1));
                } else {
                    /*int op1 = temp[i - 1][j] + 1;
                    int op2 = temp[i - 1][j - 1] + 1;
                    int op3 = temp[i][j - 1] + 1;
                    temp[i][j] = Math.min(Math.min(op1, op2), op3);*/
                    int op1 = tpl.get(i - 1).get(j) + 1;
                    int op2 = tpl.get(i - 1).get(j - 1) + 1;
                    int op3 = tpl.get(i).get(j - 1) + 1;
                    //temp[i][j] = Math.min(Math.min(op1, op2), op3);
                    tpl.get(i).set(j, Math.min(Math.min(op1, op2), op3));
                }
            }
        }
        return tpl.get(s1.length() - 1).get(s2.length() - 1);
    }

    public static double similarity(String s1, String s2) {
        return (1 - dis(s1, s2) * 1.0 / Math.max(s1.length(), s2.length()));
    }
}
